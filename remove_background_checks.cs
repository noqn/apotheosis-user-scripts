var always = new ConditionalCall("Boolean AlwaysTrue()");

foreach (var convo in Conversations)
{
    foreach (var node in convo.PlayerResponseNodes)
    {
        if (TryModifyConditional(node.Conditionals, out var result, ModifyConditional))
        {
            node.Conditionals = result;
        }
    }
}

bool ModifyConditional(ConditionalCall conditional, out ConditionalCall result)
{
    result = null;

    if (conditional.Not)
    {
        return false;
    }

    if (conditional.Data.Parameters is not ["b1a8e901-0000-0000-0000-000000000000", _])
    {
        return false;
    }

    bool ok = conditional.Data.FullName switch
    {
        "Boolean HasClass(Guid, Guid)" => true,
        "Boolean HasSubClass(Guid, Guid)" => true,
        "Boolean IsDeity(Guid, Guid)" => true,
        "Boolean IsPaladinOrder(Guid, Guid)" => true,
        "Boolean IsBackground(Guid, Guid)" => true,
        _ => false
    };
    
    if (!ok)
    {
        return false;
    }
    
    result = always with { Operator = conditional.Operator };
    return true;
}
