string[][] SubclassPairs = new[]
{
  // { Old, New }
  new[] { "Rogue_Assassin", "Wizard_Conjurer" },
  new[] { "Priest_Eothas", "Wizard_Blood_Mage" },
  new[] { "Barbarian_CorpseEater", "Wizard_Evoker" },
  // etc
};

foreach (string[] pair in SubclassPairs)
  for (int i = 0; i < 2 ; i++)
    if (TryGetGameDataWithName(pair[i], out var wrapper))
      pair[i] = wrapper.ID.ToString();
    else
      Throw($"Couldn't find {pair[i]}.");

foreach (var convo in Conversations)
  foreach (var node in convo.Nodes)
    if (TryModifyConditional(node.Conditionals, out var modified, SwapClass))
      node.Conditionals = modified;

bool SwapClass(ConditionalCall conditional, out ConditionalCall result)
{
  result = null;
  
  if (conditional.Not)
    return false;

  if (conditional.Data.FullName is not "Boolean HasSubClass(Guid, Guid)")
    return false;

  if (conditional.Data.Parameters.Count is not 2)
    return false;

  if (conditional.Data.Parameters[0] is not "b1a8e901-0000-0000-0000-000000000000")
    return false;

  string subclass = conditional.Data.Parameters[1];
  
  if (SubclassPairs.FirstOrDefault(p => p[0] == subclass) is not string[] pair)
    return false;
  
  result = conditional with {
    Data = conditional.Data with {
      Parameters = conditional.Data.Parameters.SetItem(1, pair[1])
    }
  };
  
  return true;
}
